/*
 * koapopup
 *
 *
 * Copyright (c) 2012 Kristijan Husak & Aleksandar Gosevski
 * Licensed under the MIT, GPL licenses.
 */

(function($) {

    var namespace = 'popup';

    function Popup(element, options) {
        if (!element) return null;
        var o = this.options = $.extend({}, $.fn.popup.defaults, options), closeCommand = '';
        this.callbackBeforeOpen = o.callbackBeforeOpen;
        this.callbackOpened = o.callbackOpened;
        this.callbackBeforeClosed = o.callbackBeforeClosed;
        this.callbackClosed = o.callbackClosed;
        this.$element = $(element);
        this.isOpened = false;
        
        if (!$('.popup-overlay').length) {
            $('body').append( $('<div>', { 'class': 'popup-overlay' }).css( o.overlay ).hide() );
        }
        this.$overlay = $('.popup-overlay');
        this.$popup = $(o.popupWindow).css({ position: 'absolute', 'z-index' : '999999', 'display' : 'none', 'max-width' : '100%', '-webkit-transform': 'translate3d(0,0,0)'});

        // show popup on load if it's set
        if (o.showOnload) {
            this._open(true);
        }

        // Bind function for opening popup to the element
        this.$element.off('click.' + namespace, $.proxy(this.openPopup, this)).on('click.' + namespace, $.proxy(this.openPopup, this));

        // Bind function for closing popup if setup requires 
        if (o.hidePopupOnOverlay || o.closeBtn !== null) {
            $('.popup-overlay' + ',' + o.closeBtn).on('click.' + namespace, $.proxy(this.closePopup, this));
        }

        // Setup position and overlay on window resize
        if( 'onorientationchange' in window) {
            $(window).on('orientationchange', $.proxy(this.resize, this));
        } else {
            $(window).on('resize', $.proxy(this.resize, this));
        }
    }

    Popup.prototype = {

        openPopup: function(e) {
            e.preventDefault();
            this._open();
        },

        closePopup: function(e) {
            e.preventDefault();
            this._close(e);

        },

        resize : function () {
            this.$overlay.css({
                width: $(document).width(),
                height: $(document).height()
            });

            this._setPosition();
        },
        // Position popup window at element if option position is set to 'element'
        _positionElement: function () {
            var position = this.$element.offset();

            this.$popup.css({
                top : position.top - this.$popup.innerHeight(),
                left : position.left
            });
        },
        // Position popup window at center of screen
        _positionCenter: function () {
            var o = this.options,
                position = this.$element.offset(),
                topCenter = o.forceCenter ? $(document).scrollTop() : 0,
                topVal = topCenter + window.innerHeight/2 - this.$popup.innerHeight()/2;

            this.$popup.css({
                top : ((topVal < 0) ? '10px' : topVal),
                left : window.innerWidth/2 - this.$popup.innerWidth()/2
            });
        },

        // Positioning popup window
        _setPosition: function () {
            var o = this.options;

            if (o.position === 'element') {
                this._positionElement();
            } else if (o.position === 'center') {
                this._positionCenter();
            }

            if ((window.innerHeight < this.$popup.outerHeight()) && this.isOpened) {
                $(document).scrollTop(parseInt(this.$popup.css('top'),10) - 10);
            }
        },

        _open: function(onload) {
            var self = this, o = this.options;

            this._setPosition();
            // Function called before popup open
            this.callbackBeforeOpen(this.$element);

            // If we want to prevent closing popup on overlay, we put a class on it
            if (!o.hidePopupOnOverlay) {
                this.$overlay.addClass('prevent-hide');
            } else {
                this.$overlay.removeClass('prevent-hide');
            }

            this.$overlay.css('height', $(document).height()).fadeIn('fast');
            this.$popup.fadeIn('fast', function () {
                // Function called after popup is opened
                self.callbackOpened(self.$element);
                self.isOpened = true;

                // set timeout for auto hide if options are set that way
                if (onload || o.autoHideEveryTime)
                    self._hideTimeout();
               
            });
        },

        _hideTimeout: function () {
            var self = this, o = this.options;
            if (o.hideTimeout > 0 && self.isOpened) {
                this.closeTimeout = setTimeout(function () {
                    self._close();
                }, o.hideTimeout); 
            }
        },

        _close: function (e) {
            var self = this, o = this.options, isClickedDisabledOverlay = (e !== undefined) ? $(e.currentTarget).hasClass('prevent-hide') : false;
            // If overlay click is disabled for closing, return false
            if (this.$overlay.hasClass('prevent-hide') && isClickedDisabledOverlay) return false;

            // Clearing timer for auto hide timeout
            if (o.showOnload && o.hideTimeout > 0 && this.isOpened) {
                clearTimeout(this.closeTimeout);
            }
            // Function called before popup close
            this.callbackBeforeClosed(this.$element);

            this.$popup.fadeOut(200,function () {
                if (self.isOpened) {
                    // Function called after popup is closed
                    self.callbackClosed(self.$element);
                }
                self.isOpened = false;
                
                self.$overlay.fadeOut(200, function () {
                    $(this).removeClass('prevent-hide');
                });
            });
        }
    };

    $.fn.popup = function (option) {
        return this.each(function () {
            var $this = $(this),
            data = $this.data(namespace),
            options = typeof option === 'object' && option;

            if ( !data ) {
                $this.data(namespace, (data = new Popup(this, options)));
            }

            if ( typeof option === 'string') {
                data[option]();
            }
        });
    };

    $.fn.popup.Constructor = Popup;
    $.fn.popup.defaults = {
        position : 'center', // center, element or null
        forceCenter : true,     // if true, centers in screen, otherwise, centers in document
        popupWindow : '.popup-box', // selector for popup window
        closeBtn : '.close-popup',    // additional close button
        hidePopupOnOverlay: true, // Is closing popup on overlay click enabled or disabled ? Default enabled
        showOnload: false, // show popup on page load
        hideTimeout: 0, // if showOnload or autoHideEveryTime is true, set the timeout for auto hiding after open
        autoHideEveryTime: false, // Auto hide popup after hideTimeout value every time the popup is opened
        callbackBeforeOpen: function () {}, // Callback function before popup is opened
        callbackOpened: function () {}, // Callback function after popup is opened
        callbackBeforeClosed: function () {}, // Callback function before popup is closed
        callbackClosed: function () {}, // Callback function after popup is closed
        overlay: {
            left: 0,
            top: 0,
            width: $(document).width(),
            height: $(document).height(),
            position: 'absolute',
            background: 'rgba(0,0,0,.4)',
            userSelect: 'none',
            zIndex: '99999',
            '-webkit-transform': 'translate3d(0,0,0)'
        }
    };

}(jQuery));
