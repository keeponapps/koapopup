module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({

        concat: {
            dist: {
                src: ['src/koapopup.js'],
                dest: 'dist/koapopup.js'
            }
        },

        uglify: {
            project_production: {
                files: {
                    'dist/koapopup.min.js': ['dist/koapopup.js']
                }
            }
        },

        watch: {
            files: ['src/koapopup.js'],
            tasks: ['concat', 'uglify']
        }

    });

    // Default task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['concat', 'uglify']);

};
